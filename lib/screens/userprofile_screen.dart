import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:module3_assignment1/screens/dashboard_screen.dart';

class UserProfile extends StatelessWidget {
  const UserProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("User Profile"), //Removes the back button
          centerTitle: true,
          automaticallyImplyLeading: false),
      body: Column(
        children: const [
          Text(
            "Edit your profile",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          TextField(
            decoration: InputDecoration(hintText: "Full Name"),
          ),
          TextField(
            decoration: InputDecoration(hintText: "Date of Birth"),
          ),
          TextField(
            decoration: InputDecoration(hintText: "Email"),
          ),
          TextField(
            decoration: InputDecoration(hintText: "Full Name"),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: ElevatedButton(onPressed: null, child: Text("Save")),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: ElevatedButton(onPressed: null, child: Text("Cancel")),
          ),
        ],
      ),
    );
  }
}
