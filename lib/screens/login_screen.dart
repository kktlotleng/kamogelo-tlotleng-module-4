import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:module3_assignment1/home.dart';
import 'package:module3_assignment1/screens/dashboard_screen.dart';
import 'package:module3_assignment1/screens/registration_screen.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
          title: const Text("Login"),
          centerTitle: true,
          //Removes the back button
          automaticallyImplyLeading: false),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const Text("Login into an existing account",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            const Text("Welcome back!"),
            const TextField(
                decoration: InputDecoration(hintText: "Enter your email")),
            const TextField(
              decoration: InputDecoration(hintText: "Enter your password"),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Home()));
                  },
                  child: const Text("Login")),
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const Registration()));
              },
              child: const Text("New user? Sign up."),
            ),
          ],
        ),
      ),
    );
  }
}
