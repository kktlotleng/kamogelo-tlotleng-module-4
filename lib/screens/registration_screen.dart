import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:module3_assignment1/screens/dashboard_screen.dart';
import 'package:module3_assignment1/home.dart';
import 'package:module3_assignment1/screens/login_screen.dart';

class Registration extends StatelessWidget {
  const Registration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("Registration"),
          centerTitle: true,
          //Removes the back button
          automaticallyImplyLeading: false),
      body: Padding(
        padding: const EdgeInsets.all(9.0),
        child: Column(
          children: [
            const Center(
              child: Text(
                "Create Account",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            const Text("Welcome onboard!"),
            const TextField(
                decoration: InputDecoration(hintText: "Enter your full name")),
            const TextField(
                decoration: InputDecoration(hintText: "Enter your username")),
            const TextField(
                decoration: InputDecoration(hintText: "Enter your email")),
            const TextField(
                decoration: InputDecoration(hintText: "Enter password")),
            const TextField(
              decoration: InputDecoration(hintText: "Confirm password"),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: ((context) => Home())));
                  },
                  child: const Text("Register")),
            ),
            Padding(
                padding: const EdgeInsets.all(8.0),
                child: InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => const Login()));
                  },
                  child: const Text("Already have an account? Login here."),
                )),
          ],
        ),
      ),
    );
  }
}
